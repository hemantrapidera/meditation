<?php 
   
$config['notification_add_user_in_tribe'] = array("firstName  has shared a searchName with you","add_tribes");
$config['notification_for_accept_invitation'] = array("name is accept invitation for searchName","accept_invitation");

$config['notification_for_decline_invitation'] = array("name has declined invitation for searchName","decline_invitation");

$config['notification_add_comment_on_shortlisted_property'] = array("firstName  has comment on propertyName","shortlisted_property_list");


$config['notification_add_chat_on_search'] = array("firstName  has chat on searchName","search_detail_chats");


$travelData = array(
                array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'origin',
                     'label'   => 'Orign',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'destination',
                     'label'   => 'Destination',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'departureDate',
                     'label'   => 'Departure Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|callback_itinerary_check'
                  )
            );

$config['flight'] = array(  
               
               array(
                     'field'   => 'departureTime',
                     'label'   => 'Departure Time',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'arrivalDate',
                     'label'   => 'Arrival Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'arrivalTime',
                     'label'   => 'Arrival Time',
                     'rules'   => 'required'
                  ),         
               /*array(
                     'field'   => 'pnrNo',
                     'label'   => 'PNR No',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'phoneNo',
                     'label'   => 'Phone Number',
                     'rules'   => 'required'
                  )*/
         );

$config['cruise'] = array(  
               
               array(
                     'field'   => 'departureTime',
                     'label'   => 'Departure Time',
                     'rules'   => 'required'
                  ),
               /*array(
                     'field'   => 'arrivalDate',
                     'label'   => 'Arrival Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'arrivalTime',
                     'label'   => 'Arrival Time',
                     'rules'   => 'required'
                  ),        
               array(
                     'field'   => 'cruiseLength',
                     'label'   => 'Cruise Length',
                     'rules'   => 'required'
                  ), */        
               array(
                     'field'   => 'cruiseLines',
                     'label'   => 'Cruise Lines',
                     'rules'   => 'required'
                  )
         );

$config['bus'] = array(  
               
               array(
                     'field'   => 'departureTime',
                     'label'   => 'Departure Time',
                     'rules'   => 'required'
                  ),
               /*array(
                     'field'   => 'arrivalDate',
                     'label'   => 'Arrival Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'arrivalTime',
                     'label'   => 'Arrival Time',
                     'rules'   => 'required'
                  ),       
               array(
                     'field'   => 'busType',
                     'label'   => 'Bus Type',
                     'rules'   => 'required'
                  ), */         
               array(
                     'field'   => 'busOperator',
                     'label'   => 'Bus Operator',
                     'rules'   => 'required'
                  )
         );

$config['train'] = array(  
               
               array(
                     'field'   => 'departureTime',
                     'label'   => 'Departure Time',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'arrivalDate',
                     'label'   => 'Arrival Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'arrivalTime',
                     'label'   => 'Arrival Time',
                     'rules'   => 'required'
                  )/*,        
               array(
                     'field'   => 'trainClass',
                     'label'   => 'Train Class',
                     'rules'   => 'required'
                  )*/
         );


$config['add_flight'] = array_merge($config['flight'], $travelData);

$config['add_cruise'] = array_merge($config['cruise'], $travelData);

$config['add_bus'] = array_merge($config['bus'], $travelData);

$config['add_train'] = array_merge($config['train'], $travelData);

$config['add_hotel'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'hotelName',
                     'label'   => 'Hotel Name',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'checkInDate',
                     'label'   => 'Check In Date',
                     'rules'   => 'required'
                  ),
               /*array(
                     'field'   => 'checkInTime',
                     'label'   => 'Check In Time',
                     'rules'   => 'required'
                  ),*/
               array(
                     'field'   => 'checkOutDate',
                     'label'   => 'Check Out Date',
                     'rules'   => 'required'
                  ),
               /*array(
                     'field'   => 'checkOutTime',
                     'label'   => 'Check Out Time',
                     'rules'   => 'required'
                  ),   */           
               array(
                     'field'   => 'address',
                     'label'   => 'Address',
                     'rules'   => 'required'
                  ),
               /*array(
                     'field'   => 'phoneNo',
                     'label'   => 'Phone Number',
                     'rules'   => 'required'
                  ),
                  */
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer|callback_itinerary_check'
                  )
         );

$config['add_car_rental'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'pickupAddress',
                     'label'   => 'Pickup Address',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'dropAddress',
                     'label'   => 'Drop Address',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'pickupDate',
                     'label'   => 'Pickup Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'pickupTime',
                     'label'   => 'Pickup Time',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'returnDate',
                     'label'   => 'Return Date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'returnTime',
                     'label'   => 'Return Time',
                     'rules'   => 'required'
                  ),
             /*  array(
                     'field'   => 'phoneNo',
                     'label'   => 'Phone Number',
                     'rules'   => 'required'
                  ),
            */
               array(
                     'field'   => 'companyName',
                     'label'   => 'Company Name',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer|callback_itinerary_check'
                  )
         );

$config['add_notes'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
           array(
                     'field'   => 'notesTitle',
                     'label'   => 'Notes Title',
                     'rules'   => 'required'
                  ),
           array(
                     'field'   => 'notesData',
                     'label'   => 'Notes Data',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer|callback_itinerary_check'
                  )
         );


$config['add_moment'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'momentImage',
                     'label'   => 'Image',
                     'rules'   => 'required|callback_handle_moment_pic_upload'
                  ),
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer|callback_itinerary_check'
                  )
         );

$config['add_itinerary'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'itineraryName',
                     'label'   => 'Itinerary Name',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'itineraryDate',
                     'label'   => 'Itinerary Date',
                     'rules'   => 'required'
                  )
         );

$config['fetch'] = array(
             array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
             array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer'
                  )
         );

$config['fetchItinerary'] = array(
             array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  )
         );

$config['fetch_notes'] = array(
               array(
                        'field'   => 'userId',
                        'label'   => 'User Id',
                        'rules'   => 'required|integer|callback_user_check'
                     )
               );
$config['fetch_notes1'] = array(
               array(
                        'field'   => 'userId',
                        'label'   => 'User Id',
                        'rules'   => 'required|integer|callback_user_check'
                     ),
               array(
                        'field'   => 'itineraryId',
                        'label'   => 'Itinerary Id',
                        'rules'   => 'required|integer'
                     ),
               );

/*******DELETE**********/
$config['deleteFlight'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  )
         );

$config['deleteCarRental'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  )
         );

$config['deleteHotel'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  )
         );

$config['deleteItinerary'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer|callback_itinerary_check'
                  )
         );

$config['deleteMoments'] = array(
            array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  )
         );

$config['deleteNotes'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  )
         );
$config['deleteItineraryNotes'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
           array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer'
                  )
         );
/**********EDIT*************/
$config['editFlight'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'transportId',
                     'label'   => 'Flight Id',
                     'rules'   => 'required|integer|callback_transports_check'
                  )
         );

$config['editCruise'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'transportId',
                     'label'   => 'Cruise Id',
                     'rules'   => 'required|integer|callback_transports_check'
                  )
         );

$config['editBus'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'transportId',
                     'label'   => 'Bus Id',
                     'rules'   => 'required|integer|callback_transports_check'
                  )
         );

$config['editTrain'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'transportId',
                     'label'   => 'Train Id',
                     'rules'   => 'required|integer|callback_transports_check'
                  )
         );

$config['editCarRental'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'carRentId',
                     'label'   => 'Car Rent Id',
                     'rules'   => 'required|integer|callback_car_rent_check'
                  )
         );

$config['editHotel'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'hotelId',
                     'label'   => 'Hotel Id',
                     'rules'   => 'required|integer|callback_hotel_check'
                  )
         );

$config['editItinerary'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'itineraryId',
                     'label'   => 'Itinerary Id',
                     'rules'   => 'required|integer|callback_itinerary_check'
                  )
         );
$config['editNotes'] = array(
           array(
                     'field'   => 'userId',
                     'label'   => 'User Id',
                     'rules'   => 'required|integer|callback_user_check'
                  ),
               array(
                     'field'   => 'notesId',
                     'label'   => 'Notes Id',
                     'rules'   => 'required|integer|callback_notes_check'
                  )
         );

$config['addPost'] = array(
           array(
                  'field'   => 'userId',
                  'label'   => 'User Id',
                  'rules'   => 'required|integer|callback_user_check'
               ),
            array(
                  'field'   => 'latitude',
                  'label'   => 'Latitude',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'longitude',
                  'label'   => 'longitude',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'title',
                  'label'   => 'Title',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'postCity',
                  'label'   => 'Post City',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'postCountry',
                  'label'   => 'Post Country',
                  'rules'   => 'required'
               )
         );

$config['editPost'] = array(
           array(
                  'field'   => 'userId',
                  'label'   => 'User Id',
                  'rules'   => 'required|integer|callback_user_check'
               ),
            array(
                  'field'   => 'postId',
                  'label'   => 'Post Id',
                  'rules'   => 'required|integer|callback_post_check'
               ),
            array(
                  'field'   => 'latitude',
                  'label'   => 'Latitude',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'longitude',
                  'label'   => 'longitude',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'title',
                  'label'   => 'Title',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'postCity',
                  'label'   => 'Post City',
                  'rules'   => 'required'
               ),
            array(
                  'field'   => 'postCountry',
                  'label'   => 'Post Country',
                  'rules'   => 'required'
               )
         );

$config['deletePost'] = array(
           array(
                  'field'   => 'userId',
                  'label'   => 'User Id',
                  'rules'   => 'required|integer|callback_user_check'
               )
         );

$config['add_post'] = array(
           array(
                     'field'   => 'title',
                     'label'   => 'Title',
                     'rules'   => 'required'
                  ), 
               array(
                     'field'   => 'description',
                     'label'   => 'Comment',
                     'rules'   => 'required'

                  ),
               array(
                     'field'   => 'searchInput',
                     'label'   => 'Post Location',
                     'rules'   => 'required'
                  ),
         );

$config['update_post'] = array(
       array(
                     'field'   => 'postId',
                     'label'   => 'postId',
                     'rules'   => 'required'
                  ), 
           array(
                     'field'   => 'title',
                     'label'   => 'Title',
                     'rules'   => 'required'
                  ), 
               array(
                     'field'   => 'description',
                     'label'   => 'Description',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'searchInput',
                     'label'   => 'Post Location',
                     'rules'   => 'required'
                  ),
         );

?>