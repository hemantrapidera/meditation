<?php 

class Admin_model extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();
        $this->load->library('session');

    }

    public function login()

    {

        $username = $this->input->post('email');
        $password = md5($this->input->post('password'));
        //exit;
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email', $username);
        $this->db->where('password', $password);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $row = $query->row();
            $data = array(
                'userId' => $row->adminId,
                'email' => $row->email,
                'firstName' => $row->firstName,
                'role'=>$row->role,
            );
            $this->session->set_userdata($data);
            return $query->result();
        } else {

            return false;

        } 

    }
    public function users_list(){
        $this->db->select('*');
        $this->db->from('users');
        //$this->db->where('markDelete !=', 1);
        //$this->db->where('blocked !=', 1);
        $result=$this->db->get();
        return $result->result_array();    
    }
    public function user_insert(){
        $data = array(
            'userName' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'age' => $this->input->post('age'),
            'mobile' => $this->input->post('mobile')
        );
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }
    public function select_user($id)
    {
        $this->db->select('userId,userName,email,age,mobile');
        $this->db->from('users');
        $this->db->where('userId',$id);
        $result=$this->db->get();
        return $result->result_array();
    }
    public function update_user(){
        $id = $this->input->post('id');
        $data = array(
            'userName' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'age' => $this->input->post('age'),
            'mobile' => $this->input->post('mobile')
        );
        $this->db->where('userId',$id);
       $this->db->update('users', $data);
       return true;
    }
    public function delete_user($id){

        $this->db->where('userId',$id);
        $this->db->delete('users'); 
        $this->db->last_query();
        return true;
    }
    public function category_list(){
        $this->db->select('*');
        $this->db->from('category');
        //$this->db->where('markDelete !=', 1);
        //$this->db->where('blocked !=', 1);
        $result=$this->db->get();
        return $result->result_array();    
    }
    public function add_category(){
        $data = array(
            'categoryName' => $this->input->post('categoryName')            
        );
        $this->db->insert('category', $data);
        return $this->db->insert_id(); 
    }
    public function select_category($id){
        $this->db->select('categoryId,categoryName');
        $this->db->from('category');
        $this->db->where('categoryId',$id);
        $result=$this->db->get();
        return $result->result_array();
    }
}

?>