<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public $table1 = 'users';    
  
    public function register($post_data)
    {  
        $is_register = $this->db->insert($this->table1, $post_data);
            if($is_register){
                $_POST['success'] = "Register successfully";
                return true;
            }
       
    }    
        
    public function get_facebook_user($facebookId)
    {
        $sql1 = "SELECT userId,firstName,lastName,email,profile_picture,age,gender,city,cityTravelledTo,country,countryTravelledTo,'password' loggedin_with,latitude,longitude,deviceToken,facebookId,isLoggedIn,quickBlockUserId,deleteFlag,createdAt FROM users WHERE facebookId = '".$facebookId."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {                
            return $record1->result_array();
        }        
    }
    
     public function email_check($email)
    {
        $sql1 = "SELECT userId FROM users WHERE email='".$email."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {
            return true;
        }
    }

    public function login($post_data)
    {    
        
        $email = $post_data['email'];
        $password = $post_data['password'];
        //$password = do_hash($password);
        $device_token = array_key_exists("deviceToken",$post_data);
        if($device_token){
            $device_token = $post_data['deviceToken'];
        }else{
            $device_token = '';
        }

        //$device_token = $post_data['deviceToken'];
        /* check the email is register by facebbok or not, if yes the return error */
        $check_user_exist = "SELECT * FROM users WHERE email = '".$email."' AND deleteFlag !=1";
        $result = $this->db->query($check_user_exist);
        if ($result->num_rows()>0) {
            $data = $result->result_array();
           // print_r($data);exit();
            $user_password = $data[0]['password'];
            $facebookId = $data[0]['facebookId'];
            if($user_password == ''  && $facebookId){
                $_POST['login_error'] = "This email was used to register with facebook,Please use facebook login";  
                return false;  
            }else{
                if($password == $user_password) {
                    /*Update the isLoggedIn flag and device tokens*/
                    $data_update = "UPDATE users SET isLoggedIn = 1,
                                        deviceToken ='".$device_token."'
                                        WHERE email ='".$email."'";
                    $record1 = $this->db->query($data_update);
                    $data[0]['isLoggedIn'] = 1;
                    $data[0]['deviceToken'] = $device_token;
                    return $data;          
                }else{
                    $_POST['login_error'] = "Password not valid"; 
                    return false;   
                }
            }
        }else{
            $_POST['login_error'] = "Please enter a valid email and password"; 
            return false;
        }        
    }

    public function logout($post_data)
    {
        $userId = $post_data['userId'];
        $sql1 = "SELECT userId FROM users WHERE userId = '".$userId."'";
        $record1 = $this->db->query($sql1);
        if ($record1->num_rows()>0) {

                $device_token_update = "UPDATE users SET deviceToken ='', isLoggedIn = 0 WHERE userId='".$userId."'";
                $result = $this->db->query($device_token_update);                
            return $result;
        }

    }

    /*function for make sure the  student id exists*/
    public function user_check($userId)
    {
        $sql1 = "SELECT userId FROM users WHERE userId ='".$userId."'";
        $record = $this->db->query($sql1);
        if ($record->num_rows()>0) {
            return true;
        }   
    }  
  
    /* Save tokens */
    public function save_token_with_expiry($post_data,$email)
    {
        if (empty($post_data['latitude'])) {
            unset($post_data['latitude']);
        }
        if (empty($post_data['longitude'])) {
            unset($post_data['longitude']);
        }
        unset($post_data['email']);
        unset($post_data['password']);
        unset($post_data['profile_picture']);
        return $this->db->where('email',$email)->update($this->table1, $post_data);
    }

    public function refresh_token_check($refresh_token)
    {
        $sql = "SELECT refreshTokenExpiry FROM users WHERE refreshToken='".$refresh_token."'";
        $record = $this->db->query($sql);        
        if ($record->num_rows()>0) {
            return $record->row('refreshTokenExpiry');            
        }
    }

    public function access_token($post_data)
    {
        if (empty($post_data['latitude'])) {
            unset($post_data['latitude']);
        }
        if (empty($post_data['longitude'])) {
            unset($post_data['longitude']);
        }

        return $this->db->where('refreshToken',$post_data['refreshToken'])->update($this->table1, $post_data);
    }
	
	
    /*****************************************************************************************/

    /*function for check email exists or not */
     public function email_exists($email)
    {
        $sql1 = "SELECT userId FROM users WHERE email='".$email."'";
        $record1 = $this->db->query($sql1);        
        if ($record1->num_rows()>0) {
            return true;
        }else
        {
           return false;
        }
        
    }

    /* forgot password */
    public function fogotPassword($post_data)
    {
        $email = $post_data['email'];
        $sql = "SELECT userId,userName,email FROM users WHERE email='".$email."'";
        $record = $this->db->query($sql);
        if ($record->num_rows()>0) {
                //return $record->row('student_id');
                return $record->result_array();
        }else{
                return false;
            }
    }    
    
    

    
}?>