<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./application/libraries/REST_Controller.php');
//require_once('./application/libraries/new_stripe/vendor/autoload.php');
//require_once APPPATH.'';
//require_once('./application/libraries/new_stripe/vendor/autoload.php');  
require_once("./application/core/MY_Controller1.php");

class Common extends MY_Controller1 {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('my_constants');
		$this->load->model('mobile_api/Common_model');
		$this->load->model('mobile_api/Auth_model');
		$this->load->helper('security');	
		include APPPATH . 'libraries/classes/class.phpmailer.php';
	}

	
	/*Check the user present or not*/
	public function user_check($userId)
	{	
		$status = $this->Common_model->user_check($userId);	
		if ($status){
			return true;            
		}else{
			$this->form_validation->set_message('user_check', '{field} is do not exists');
			return false;
		}
	}

	/* Student Profile details */
	public function profileDetails_post()
	{
		$post_data = $this->post();
		
		$this->form_validation->set_data($post_data);
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('userId','User Id','required|numeric|callback_user_check');

		if ($this->form_validation->run() === false) {
    			$this->response(array('ResponseCode' => 0, 'ResponseMessage' => 'FAILURE', 'Comments' => validation_errors(),'Result'=>''), 400);    		
		}else{

			$profile_detail = $this->Common_model->profile_detail($post_data);
			if ($profile_detail) {
				$this->response(array('ResponseCode' => 1, 'ResponseMessage' => 'SUCCESS', 'Comments' => 'Profile detail fetched successfully','Result'=>$profile_detail[0]), 200);
			}else{
				$this->response(array('ResponseCode' => 0, 'ResponseMessage' => 'FAILURE', 'Comments' => 'No Data Found','Result'=>'No Data Found'), 400);
			}	
		}
	}

	
	
	
}?>