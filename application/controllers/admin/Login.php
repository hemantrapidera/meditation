
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Login Extends CI_Controller{

	function __construct()
	{
        parent::__construct();	
        $this->load->library('form_validation');
        $this->load->helper('form');
		//$this->config->load('my_constants');		
       // $this->load->helper('security');
		//$this->load->model('admin_api/Login_model');
        //$this->config->load('my_constants');		
        $this->load->helper('url');	
        $this->load->model('admin/Admin_model');
	}
/*Admin Login Page*/
	public function index()
	{	
		
		$data['title'] = 'Login';
		$this->load->view('admin/login_view',$data);
		
    }
    /* Admin Login details page check  function */ 
    
    public function validate_user()
	{	
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        //$this->load->model('admin_model');
        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/login_view');
        } else {

            $result = $this->Admin_model->login();

            if (!$result) {

                $this->session->set_flashdata('login_failure', 'Your Login Was Unsuccessful - Please Try Again.');

                redirect('admin/Login/index');
            } else {

                redirect('admin/Login/dashboard');
            }
        }
		
    }
    /* Dashboard firstpage after admin login */
    public function dashboard(){
        $data['title'] = 'Dashboard';
        //$this->load->view('admin/dashboard',$data);
        $data['main_content'] = 'admin/dashboard';
        $this->load->view('admin/includes/template', $data);

    }
    /*Users List*/
    public function users(){
        $data['users'] = $this->Admin_model->users_list();
        $data['title'] = 'Users';
        $data['main_content'] = 'admin/users';
        $this->load->view('admin/includes/template', $data);

    }
    public function add_user(){
        $data['title'] = 'Dashboard';
        //$this->load->view('admin/dashboard',$data);
        $data['main_content'] = 'admin/add_user';
        $this->load->view('admin/includes/template', $data);
    }
    public function insert_user(){
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('age', 'Age', 'trim|required');
        $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['main_content'] = "admin/add_user";
            $this->load->view("admin/includes/template", $data);
        } else {
           $result = $this->Admin_model->user_insert();
           if(!$result){
            $this->session->set_flashdata('signup_failure', 'Your Registration Was Unsuccessful - Please Try Again.');
   
           }else{
            redirect(base_url('admin/Login/users'), 'refresh');
           }
        }
    }
    public function edit_user($id){
        $data['user']=$this->Admin_model->select_user($id);
        //$data['speciality'] = $this->Admin_model->speciality_list();
        $data['main_content'] = "admin/edit_user";
        $this->load->view("admin/includes/template", $data);
    }
    public function update_user(){
        $result = $this->Admin_model->update_user();
        redirect(base_url('admin/Login/users'), 'refresh');
    }
    public function delete_user($id){
        $result = $this->Admin_model->delete_user($id);
        //$data['speciality'] = $this->Admin_model->speciality_list();
        //$data['main_content'] = "admin/edit_user";
        redirect(base_url('admin/Login/users'), 'refresh');
    }
    public function category(){
        $data['categories'] = $this->Admin_model->category_list();
        $data['title'] = 'Category';
        $data['main_content'] = 'admin/category';
        $this->load->view('admin/includes/template', $data);

    }
    public function add_category(){
        $data['title'] = 'Dashboard';
        //$this->load->view('admin/dashboard',$data);
        $data['main_content'] = 'admin/add_category';
        $this->load->view('admin/includes/template', $data);
    }
    public function insert_category(){
        $this->form_validation->set_rules('categoryName', 'Category Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['main_content'] = "admin/add_category";
            $this->load->view("admin/includes/template", $data);
        } else {
           $result = $this->Admin_model->add_category();
           if(!$result){
            $this->session->set_flashdata('signup_failure', 'Your Registration Was Unsuccessful - Please Try Again.');
   
           }else{
            redirect(base_url('admin/Login/category'), 'refresh');
           }
        }
    }
    public function edit_category($id){
        $data['category']=$this->Admin_model->select_category($id);
        //$data['speciality'] = $this->Admin_model->speciality_list();
        $data['main_content'] = "admin/edit_category";
        $this->load->view("admin/includes/template", $data);
    }

	
}?>